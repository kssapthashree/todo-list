<img src="images/IDSN-new-logo.png"  width="400"/>

# Hands-on Lab - React Todo_List Application

## Estimated Time Needed: 1 hour 30 mins

You will learn how to develop a **to-do** list application, using React, in this lab. Your application will allow the addition of tasks to the to-do list, edition them, and then deletion of the completed tasks.

**Objective:** <br>

By the end of this lab, you will know how to create simple stylized responsive web applications with React.

# Set-up : Fork and clone the repository

1. Go to the  git repository https://github.com/ibm-developer-skills-network/uqwxd-react_todolist.git that contains the starter code needed for this lab and fork it to your repository.


2. In the lab environment, open a terminal window by choosing Terminal > New Terminal from the menu.

<img src="images/new-terminal.png" style="align:center" width="75%"/>


3. Change to your project folder, if you are not in the project folder already.

```
cd /home/project
```

{: codeblock}

4. Clone your forked Git repository, if it doesn't already exist.

```
[ ! -d 'uqwxd-react_todolist' ] && git clone https://github.com/<your Github username>/uqwxd-react_todolist.git
```
{: codeblock}

5. Change to the directory uqwxd-react_todolist directory to start working on the lab.

```
cd uqwxd-react_todolist/
```

{: codeblock}

6.  List the contents of this directory to see the artifacts for this lab.

```
ls 
```
<img src="images/ls-command.jpg" style="align:center" width="75%"/>

{: codeblock}

# Exercise 1: Installing and running the server for React Application

The structure of the project folder and the starter code for To-Do list is shown in the below screenshot.

<img src="images/filestructure.jpg" style="align:center" width="75%"/>

1. In the file Explorer go to App.js file under src folder and view it. It would appear like this.

<img src="images/startercode.jpg" style="align:center" width="75%"/>

2. When you initialize the variable for the input field using the useState Hook, you define a getter, to get the value of the state and a setter, to set the value of the state. In the code above, **todos** is the state, and **setTodos** is the function that updates the state value. Similarly we have another state **todo** with its own setter. 

```
const [todos, setTodos] = React.useState([]);
const [todo, setTodo] = React.useState("");
```

3. In the terminal, ensure you are in the **uqwxd-react_todolist** directory and run the following command to install all the packages that are required for running the server.

```sh
npm install
```

>This will install all the required packages as defined in packages.json.

4. Start the server using the below command in the terminal.

```sh
npm start
```
You will see this output indicating that the server is running.

<img src="images/npm_start.jpg" style="align:center" width="75%"/>

5. To verify that the server is running, click on the Skills Network button on the left to open the Skills Network Toolbox. Then click **Other**. Choose Launch Application and enter the port number 3000 on which the server is running and click the launch icon. 

<img src="images/Launch_Application-SN-ToolBox.png" style="align:center" width="75%"/>

The todoapp UI will appear on the browser as seen in the image below.

<img src="images/ex1output.jpg" style="align:center" width="75%"/>

6. To stop the server, go to the terminal in the lab environment and press Ctrl+c to stop the server.


# Exercise 2: Create a New Todo task.

1. Now, let us give application the power to add a new task for our to-do list app. You will add a handleSubmit function that can handle newTodo items and add the task to the list. The user input is validated to ensure the input is non-empty and doesn't have preceeding or succeeding spaces. 

```
function handleSubmit(e) {
    e.preventDefault();

    const newTodo = {
      id: new Date().getTime(),
      text: todo.trim(),
      completed: false,
    };
    if (newTodo.text.length > 0 ) {
        setTodos([...todos].concat(newTodo));
        setTodo("");
    
    } else {
        
        alert("Enter Valid Task");
        setTodo(""); 
    }
  }
```
{: codeblock}

<img src="images/addtask.jpg" style="align:center" width="75%"/>

2. On submitting the form, the task is added to the todo array. The code uses the map to iterate through the todo array, and renders each task as a list item. Using useState, this component registers a state — value — and a function for updating it — setTodo. The handleSubmit handler will prevent the default action that would normally be taken on the form and add a new Task using the latest value that is in the input field. 

Paste the below code inside the return function as shown in the screenshot.

```
<h1>Todo List</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          onChange={(e) => setTodo(e.target.value)}
          placeholder="Add a new task"
          value={todo}
        />
            <button type="submit">Add Todo</button>
        </form>
        {todos.map((todo) => <div>{todo.text}</div>)}
```
{: codeblock}

<img src="images/returnadd1.jpg" style="align:center" width="75%"/>

3.Start the server and enter the valid task inside the input box where you can see "Add a new task ". You will see the ouput as below:

<img src="images/addoutput.jpg" style="align:center" width="75%"/>


# Exercise 3: Delete a completed task from the list.

1. Task deletion can be handled in many ways. In the code sample below, you will create the filter method that will be applied when a button is clicked that filters out the task to be deleted and returns the rest of the tasks.

```
function deleteTodo(id) {
    let updatedTodos = [...todos].filter((todo) => todo.id !== id);
    setTodos(updatedTodos);
  }

```
{:codeblock}

<img src="images/deletetdo.jpg" style="align:center" width="75%"/>

2. Add a button to delete the task.

```
<button onClick={() => deleteTodo(todo.id)}>Delete</button>
```

<img src="images/deletebutton.jpg" style="align:center" width="75%"/>

3. Start the server, add the task to-do list, and then try to delete it by pressing the delete button. The task must be removed from the list.

<img src="images/deleteoutput.jpg" style="align:center" width="75%"/>


# Exercise 4: Adding Checkbox and Toggle function:

1. You will now add a checkbox to mark task completion. Create a new function, **toggleComplete** function that uses map method to iterate through the task and mark them complete.

```
function toggleComplete(id) {
    let updatedTodos = [...todos].map((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    });
    setTodos(updatedTodos);
  }
```
{:codeblock}

<img src="images/toggle.jpg" style="align:center" width="75%"/>

Code to add the checkbox
```
<input type ="checkbox" onChange={()=>toggleComplete(todo.id)} checked={todo.completed}/>
```
{:codeblock}

<img src="images/checkbox.jpg" style="align:center" width="75%"/>

2. Start the server, add the task to-do list, and then use the checkbox if the task is completed.

<img src="images/toggleoutput.jpg" style="align:center" width="75%"/>


# Exercise 5: Edit a added Todo task and submit it.

In the previous exercise, you added the ability to toggle the checkbox for completed task in todos. Now add the functionality to edit the task.

You need to add two more states to implement the editing functionality.

  const [todoEditing, setTodoEditing] = React.useState(null);
  const [editingText, setEditingText] = React.useState("");

```
function submitEdits(id) {
  const updatedTodos = [...todos].map((todo) => {
    if (todo.id === id) {
      todo.text = editingText;
      }
      return todo;
    });
    setTodos(updatedTodos);
    setTodoEditing(null);
  }

```
<img src="images/submitedit.jpg" style="align:center" width="75%"/>

2. You will be allowing edits only in the editing mode. Check for the appropriate mode, then display the editing form where one can edit the to-do list and submit it back to the list using the ternary operator. The editing form has a couple of additional buttons so the user can control between submitting an edit and deleting the task.

```
{todo.id === todoEditing ? (
<input type="text" onChange={(e) => setEditingText(e.target.value)} />
) : (
<div>{todo.text}</div>
)}
</div>
<div className="todo-actions">
{todo.id === todoEditing ? (
<button onClick={() => submitEdits(todo.id)}>Submit Edits</button>
) : (
<button onClick={() => setTodoEditing(todo.id)}>Edit</button>
)}
```
<img src="images/editbutton.jpg" style="align:center" width="75%"/>

3. Start the server, add the task to-do list and edit it using the edit button.

<img src="images/editoutput.jpg" style="align:center" width="75%"/>


# Exercise 6: Adding the useEffect hook

1. You will use the useEffect hook to add to our application. This useEffect hook will be responsible to save new todos into localstorage.

```
React.useEffect(() => {
    const json = localStorage.getItem("todos");
    const loadedTodos = JSON.parse(json);
    if (loadedTodos) {
      setTodos(loadedTodos);
    }
  }, []);

  React.useEffect(() => {
    const json = JSON.stringify(todos);
    localStorage.setItem("todos", json);
  }, [todos]);

```
{:codeblock}


## The wholistic view

Final code To-do list is as shown below after you put all the parts together, will be as below. 

```
import React from "react";
import "./App.css";

const App = () => {
  const [todos, setTodos] = React.useState([]);
  const [todo, setTodo] = React.useState("");
  const [todoEditing, setTodoEditing] = React.useState(null);
  const [editingText, setEditingText] = React.useState("");

  React.useEffect(() => {
    const json = localStorage.getItem("todos");
    const loadedTodos = JSON.parse(json);
    if (loadedTodos) {
      setTodos(loadedTodos);
    }
  }, []);

  React.useEffect(() => {
    const json = JSON.stringify(todos);
    localStorage.setItem("todos", json);
  }, [todos]);

  function handleSubmit(e) {
    e.preventDefault();

    console.log("...."+ todo + "....");

    const newTodo = {
      id: new Date().getTime(),
      text: todo.trim(),
      completed: false,
    };
    
    console.log("...."+ newTodo.text + "....");

      if (newTodo.text.length > 0 ) {
        //alert("Ok");
        setTodos([...todos].concat(newTodo));
        setTodo("");
    
    } else {
        
        alert("Enter Valid Items");
        setTodo("");
        
    }
  }
  

  function deleteTodo(id) {
    let updatedTodos = [...todos].filter((todo) => todo.id !== id);
    setTodos(updatedTodos);
  }

  function toggleComplete(id) {
    let updatedTodos = [...todos].map((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    });
    setTodos(updatedTodos);
  }

  function submitEdits(id) {
    const updatedTodos = [...todos].map((todo) => {
      if (todo.id === id) {
        todo.text = editingText;
      }
      return todo;
    });
    setTodos(updatedTodos);
    setTodoEditing(null);
  }

  return (
    <div id="todo-list">
      <h1>Todo List</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          onChange={(e) => setTodo(e.target.value)}
          value={todo}
        />
        <button type="submit">Add Todo</button>
      </form>
      {todos.map((todo) => (
        <div key={todo.id} className="todo">
          <div className="todo-text">
            <input
              type="checkbox"
              id="completed"
              checked={todo.completed}
              onChange={() => toggleComplete(todo.id)}
            />
            {todo.id === todoEditing ? (
              <input
                type="text"
                onChange={(e) => setEditingText(e.target.value)}
              />
            ) : (
              <div>{todo.text}</div>
            )}
          </div>
          <div className="todo-actions">
            {todo.id === todoEditing ? (
              <button onClick={() => submitEdits(todo.id)}>Submit Edits</button>
            ) : (
              <button onClick={() => setTodoEditing(todo.id)}>Edit</button>
            )}

            <button onClick={() => deleteTodo(todo.id)}>Delete</button>
          </div>
        </div>
      ))}
    </div>
  );
};

export default App;


```

2. Please commit and push all the changes to your forked Github repo.

> Note: Please refer to [this](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CD0101EN-SkillsNetwork/labs/GitHubLabs/Git_Clone_Commit_Push.md.html?origin=www.coursera.org) lab, if you need any help in committing and pushing changes to your repo.


### Congratulations! You have completed the lab for creating To-Do list Application using React.

## Summary

In this lab, we used React to build a to-do list application that allows us to view the list of tasks, add tasks, edit tasks, and delete tasks.

## Author(s)
<h4> Sapthashree K S <h4/>


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2022-10-10 | 1.0  | Sapthashree K S | Initial version created based videos |

